export const WellbeingInfo = {
  appId: {
    Business: 16,
    Free: 15,
    Kids: 18,
    Sport: 17,
  },
  categoryId: {
    'inspire-me': 21,
    'relax-me': 13,
    'teach-me': 22,
  },
  contentTypeId: {
    video: 14,
    publication: 19,
    audio: 20,
  }
}
