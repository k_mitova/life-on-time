import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { Goal } from 'app/shared/models/goal';
import { GoalCreate } from 'app/shared/models/goalCreate';
import { TaskCreate } from 'app/shared/models/taskCreate';
import { ActionInfo } from 'app/shared/models/actionInfo';
import { ItemInfo } from 'app/shared/models/itemInfo';
import { Category } from 'app/shared/models/category';

import { GoalService } from 'app/shared/services/goal.service';
import { TaskService } from 'app/shared/services/task.service';
import { UserService } from 'app/shared/services/user.service';
import { ModalService } from "app/shared/services/modal.service";
import { EventService } from 'app/shared/services/event.service';

import { convertDateToString } from "app/shared/utilities";



@Component( {
  selector: 'app-goals-page',
  templateUrl: './goals-page.component.html',
  styleUrls: [ './goals-page.component.scss' ]
} )
export class GoalsPageComponent implements OnInit, OnDestroy {
  @ViewChild( 'type', { static: false } ) type: any;

  currentGoalCategory: Category;
  goalCategories: Array<Category>;
  private path: string;
  goals$: Observable<Array<Goal>>;
  private goalCategoriesSubscription: Subscription;
  private deleteSubscription: Subscription;
  private createGoalSubscription: Subscription;
  private createTaskSubscription: Subscription;
  private editGoalSubscription: Subscription;
  private editTaskSubscription: Subscription;
  private modalCreateSubscription: Subscription;
  private modalDeleteSubscription: Subscription;
  private modalStatusSubscription: Subscription;
  private pathSubs: Subscription;

  constructor (
    private modalService: ModalService,
    private route: ActivatedRoute,
    private goalService: GoalService,
    private taskService: TaskService,
    private eventService: EventService,
    private userService: UserService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.pathSubs = this.route.url.subscribe( data => {
      this.path = data[ 0 ].path;
      this.goalCategoriesSubscription = this.userService.getUserAvailableCategoriesAndUserAppType()
        .subscribe( data => {
          this.goalCategories = data;
          this.currentGoalCategory = this.goalCategories
            .find( category => category.pathEnd === this.path );
          if ( !this.currentGoalCategory ) {
            this.router.navigate( [ "/error" ] )
          } else {
            this.loadPageGoals();
          }
        } );
    } )
    this.modalCreateSubscription = this.eventService.on( 'confirm create/edit', ( actionInfo => this.mapAction( actionInfo ) ) );
    this.modalDeleteSubscription = this.eventService.on( 'confirm delete', ( itemInfo => this.deleteItem( itemInfo ) ) );
    this.modalStatusSubscription = this.eventService.on( 'change status', ( itemInfo => this.changeStatus( itemInfo ) ) )
  }

  private mapAction( actionInfo: ActionInfo ) {
    if ( actionInfo.actionType === 'create' ) {
      if ( actionInfo.itemType === 'goal' ) {
        this.createGoal( actionInfo.formValue );
      } else if ( actionInfo.itemType === 'action' ) {
        this.createTask( actionInfo.formValue, actionInfo.goalId )
      }

    } else if ( actionInfo.actionType === 'edit' ) {
      if ( actionInfo.itemType === 'goal' ) {
        this.editGoal( actionInfo.formValue, actionInfo.itemId );
      } else if ( actionInfo.itemType === 'action' ) {
        this.editTask( actionInfo.formValue, actionInfo.itemId );
      }
    }
  }

  private createGoal( formValue ) {
    let goal: GoalCreate = formValue;
    const date = formValue.until_date;
    goal.until_date = convertDateToString( date.day, date.month, date.year, '-' );
    goal.category_id = this.currentGoalCategory.id;
    this.createGoalSubscription = this.goalService.postCreateGoal( goal )
      .subscribe( data => {
        this.loadPageGoals();
      } )
  }

  private createTask( formValue, goalId: string ) {
    let task: TaskCreate = formValue;
    task.goal_id = goalId;
    const date = formValue.until_date;
    task.until_date = convertDateToString( date.day, date.month, date.year, '-' );

    this.createTaskSubscription = this.taskService.postCreateTask( task )
      .subscribe( data => {
        this.loadPageGoals();
      } )
  }

  private editGoal( formValue, goalId: string ) {
    const goal: GoalCreate = formValue;
    const date = formValue.until_date;
    goal.until_date = convertDateToString( date.day, date.month, date.year, '-' )
    this.editTaskSubscription = this.goalService.putEditGoalById( goalId, goal )
      .subscribe( data => {
        this.loadPageGoals();
      } )
  }

  private editTask( formValue, taskId: string ) {
    let task: TaskCreate = formValue;
    const date = formValue.until_date;
    task.until_date = convertDateToString( date.day, date.month, date.year, '-' )
    this.editTaskSubscription = this.taskService.putEditTaskById( taskId, task )
      .subscribe( data => {
        this.loadPageGoals();
      } )
  }

  private deleteItem( itemInfo: ItemInfo ) {
    if ( itemInfo.itemType === 'goal' ) {
      this.deleteSubscription = this.goalService.deleteGoalById( itemInfo.itemId )
        .subscribe( data => {
          this.loadPageGoals();
        } )
    } else if ( itemInfo.itemType === 'action' ) {
      this.deleteSubscription = this.taskService.deleteTaskById( itemInfo.itemId )
        .subscribe( data => {
          this.loadPageGoals();
        } )
    }
  }

  private changeStatus( itemInfo: any ) {
    const item = {
      id: itemInfo.itemId,
      status: itemInfo.status === 0 ? 1 : 0
    }
    if ( itemInfo.itemType === 'goal' ) {
      this.editGoalSubscription = this.goalService.putEditGoalById( itemInfo.itemId, item )
        .subscribe( data => {
          this.loadPageGoals();
        } );
    } else if ( itemInfo.itemType === 'action' ) {
      this.editTaskSubscription = this.taskService.putEditTaskById( itemInfo.itemId, item )
        .subscribe( data => {
          this.loadPageGoals();
        } );
    }
  }

  private loadPageGoals() {
    this.goals$ = this.goalService.getGoalsByCategory( this.currentGoalCategory.title );
  }

  openModal( name: string, itemType: string, actionType: string, item?: any ) {
    this.modalService.open( name, itemType, actionType, item );
  }

  ngOnDestroy() {
    if ( this.pathSubs ) {
      this.pathSubs.unsubscribe();
    }
    if ( this.deleteSubscription ) {
      this.deleteSubscription.unsubscribe();
    }
    if ( this.createGoalSubscription ) {
      this.createGoalSubscription.unsubscribe();
    }
    if ( this.createTaskSubscription ) {
      this.createTaskSubscription.unsubscribe()
    }
    if ( this.editGoalSubscription ) {
      this.editGoalSubscription.unsubscribe();
    }
    if ( this.editTaskSubscription ) {
      this.editTaskSubscription.unsubscribe()
    }
    if ( this.modalCreateSubscription ) {
      this.modalCreateSubscription.unsubscribe()
    }
    if ( this.modalDeleteSubscription ) {
      this.modalDeleteSubscription.unsubscribe();
    }
    if ( this.modalStatusSubscription ) {
      this.modalStatusSubscription.unsubscribe()
    }
    if ( this.goalCategoriesSubscription ) {
      this.goalCategoriesSubscription.unsubscribe()
    }
  }
}
